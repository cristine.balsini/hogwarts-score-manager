import { combineReducers } from "redux";
import getData from "./reduce-fetch.js";
import housePoints from "./add-remove-points.js";

export default combineReducers({ housePoints, getData });
