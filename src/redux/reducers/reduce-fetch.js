import { API_DATA } from "../actions";

const defaultState = {
  name: "",
  house: "",
  image: "",
};

const getData = (state = defaultState, action) => {
  switch (action.type) {
    case API_DATA:
      return {
        ...state,
        state: action.data.map((item) => ({
          name: item.name,
          house: item.house,
          image: item.image.replace("http", "https"),
        })),
      };
    default:
      return state;
  }
};

export default getData;
