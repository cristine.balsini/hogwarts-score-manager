export const API_DATA = "API_DATA";

export const getDataAPI = () => (dispatch) => {
  fetch("https://hp-api.herokuapp.com/api/characters/students")
    .then((resp) => resp.json())
    .then((resp) => {
      dispatch({
        type: API_DATA,
        data: resp,
      });
    });
};

export const addHousePoints = (house, name, image, points) => ({
  type: "ADD",
  house: house,
  name: name,
  image: image,
  points: points,
});

export const removeHousePoints = (points, house) => ({
  type: "REMOVE",
  house: house,
  points: points,
});
