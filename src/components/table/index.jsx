import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getDataAPI, addHousePoints } from "../../redux/actions";
import Modal from "../modal";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { FaScroll } from "react-icons/fa";

const StudentTable = () => {
  // ** material ui ** //
  const useStyles = makeStyles({
    container: {
      backgroundColor: "#C5CDDB",
      marginTop: "2rem",
      padding: "1.6rem",
      boxShadow: "0 0 7px #5777B8",
    },
    cell: {
      fontSize: "1.1rem",
      color: "#5777A8",
      borderBottom: "1px solid #5777A8",
    },
    mainCell: {
      fontSize: "1.5rem",
      color: "#5777A8",
      lineHeight: "8rem",
      borderBottom: "0",
    },
    titleCell: {
      fontSize: "1.1rem",
      fontWeight: "bolder",
      color: "#5777A8",
      borderBottom: "1px solid #5777A8",
    },
  });
  const classes = useStyles();
  // ** material ui ** //

  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();
  const studentsData = () => dispatch(getDataAPI());
  const state = useSelector((state) => state.getData.state);

  useEffect(studentsData, []);

  return (
    <Grid container direction="row" justify="center" alignItems="center">
      <Grid item xs="11">
        <TableContainer component={Paper} classes={{ root: classes.container }}>
          <Modal showModal={showModal} setShowModal={setShowModal} />
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell classes={{ root: classes.mainCell }}>
                  Students
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell classes={{ root: classes.titleCell }}>
                  Name
                </TableCell>
                <TableCell classes={{ root: classes.titleCell }}>
                  House
                </TableCell>
                <TableCell classes={{ root: classes.titleCell }}></TableCell>
              </TableRow>
            </TableHead>
            <TableBody >
              {state !== undefined &&
                state.map(({ name, house, image }, index) => (
                  <TableRow key={index}>
                    <TableCell align="left" classes={{ root: classes.cell }}>
                      {name}
                    </TableCell>
                    <TableCell align="left" classes={{ root: classes.cell }}>
                      {house}
                    </TableCell>
                    <TableCell align="right" classes={{ root: classes.cell }}>
                      <button
                        onClick={() => {
                          setShowModal(true);
                          dispatch(addHousePoints(house, name, image, 0));
                        }}
                        style={{ border: 0, backgroundColor: "#C5CDDB" }}
                      >
                        <FaScroll style={{ fontSize: 40, color: "#5777A8" }} />
                      </button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};
export default StudentTable;
