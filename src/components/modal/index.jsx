import React, { useState } from "react";
import {
  DivModal,
  FlexDiv,
  Image,
  ButtonGain,
  ButtonLose,
  ButtonDone,
} from "../../styles/modal-style.js";
import { useSelector, useDispatch } from "react-redux";
import { addHousePoints, removeHousePoints } from "../../redux/actions";

const Modal = ({ showModal, setShowModal }) => {
  const [doneModal, setDoneModal] = useState(false);
  const [points, setPoints] = useState(0);
  const [mathOperator, setMathOperator] = useState(false);
  const dispatch = useDispatch();

  const student = useSelector((state) => state.housePoints);

  const handleSubmit = (e) => {
    e.preventDefault();
    setPoints(points);
    dispatch(
      addHousePoints(student.house, student.name, student.image, points)
    );

    setShowModal(false);
    setDoneModal(true);
    setMathOperator(false);
  };

  return (
    <>
      {showModal && !doneModal && (
        <DivModal>
          <FlexDiv>
            <Image alt="student" src={student.image} />
            <div>
              <svg
                width="70"
                height="100"
                viewBox="0 0 298 361"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d={student.logoModal} fill="#5777A8" />
              </svg>
              <h4 style={{ fontSize: 30 }}>{student.house}</h4>
              <h3 style={{ fontSize: 50 }}>{student.name}</h3>

              <input
                type="number"
                value={points}
                onChange={(e) => setPoints(parseInt(e.target.value))}
                style={{ height: 25, width: 290, fontSize: 20 }}
              />
              <div style={{ padding: 10 }}>
                <ButtonGain onClick={handleSubmit}>GAIN</ButtonGain>
                <ButtonLose
                  onClick={() => {
                    setShowModal(false);
                    dispatch(removeHousePoints(points, student.house));
                    setDoneModal(true);
                    setMathOperator(true);
                  }}
                >
                  LOSE
                </ButtonLose>
              </div>
            </div>
          </FlexDiv>
        </DivModal>
      )}
      {doneModal && (
        <DivModal>
          <FlexDiv>
            <Image alt="student" src={student.image} />
            <div>
              <svg
                width="70"
                height="100"
                viewBox="0 0 298 361"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d={student.logoModal} fill="#5777A8" />
              </svg>
              <h4 style={{ fontSize: 30 }}>{student.house}</h4>
              <h3 style={{ fontSize: 50 }}>{student.name}</h3>
              {!mathOperator ? (
                <h2 style={{ color: " #65E1CB", fontSize: 60 }}>+ {points}</h2>
              ) : (
                <h2 style={{ color: "#F8A388", fontSize: 60 }}>- {points}</h2>
              )}
              <div>
                <ButtonDone
                  onClick={() => {
                    setDoneModal(false);
                    setPoints(0);
                  }}
                >
                  DONE
                </ButtonDone>
              </div>
            </div>
          </FlexDiv>
        </DivModal>
      )}
    </>
  );
};

export default Modal;
