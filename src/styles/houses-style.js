import styled from "styled-components";

export const School = styled.div`
  min-width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  background-color: whitesmoke;

  @media screen and (max-width: 320px) {
    margin-left: 10rem;
  }
  @media screen and (min-width: 375px) {
    margin-left: 7rem;
  }
  @media screen and (min-width: 425px) {
    margin-left: 4rem;
  }
  @media screen and (min-width: 768px) {
    margin-left: 0rem;
  }
`;

export const SchoolCard = styled.div`
  padding: 1rem;
  font-size: 2rem;
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
  @media screen and (max-width: 425px) {
  }
`;

export const Title = styled.div`
  background-color: #3461a4;
  height: 158px;
  display: flex;
  font-size: 1.5rem;
  justify-content: flex-start;
  line-height: 10rem;
  @media screen and (max-width: 425px) {
    width: 489px;
    height: 158px;
  }
`;

export const Card = styled.div`
  color: #5777a8;
  margin-top: 1rem;
  background-color: #c5cddb;
  width: 380px;
  height: 700px;
  box-shadow: 0 0 10px #5777a8;

  @media screen and (max-width: 425px) {
    width: 350px;
    height: 700px;
  }
`;

export const Hogwarts = styled.div`
  font-size: 50px;
  color: white;
  margin-left: 20px;
  font-weight: bold;
  @media screen and (max-width: 425px) {
    font-size: 20px;
  }
`;
