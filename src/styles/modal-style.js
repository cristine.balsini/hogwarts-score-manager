import styled from "styled-components";

export const DivModal = styled.div`
  position: absolute;
  padding: 10px;
  width: 776px;
  height: 489px;
  left: calc(50vw - 350px);
  top: calc(100vh - 244px);
  color: #5777a8;
  background-color: #e5edf2;
  box-shadow: 0 0 15px #4b6373;

  @media screen and (max-width: 425px) {
    width: 360px;
    height: 1000px;
    left: calc(50vw - 100px);
    top: calc(80vh - 100px);
    background-color: #e5edf2;
    text-align: center;
  }
`;

export const FlexDiv = styled.div`
  display: flex;
  justify-content: space-around;
  align-itens: center;

  @media screen and (max-width: 425px) {
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: center;
    align-itens: center;
  }
`;

export const Image = styled.img`
  width: 340px;
  height: 489px;
  left: 527px;
  top: 787px;

  @media screen and (max-width: 425px) {
    width: 360px;
  }
`;

export const ButtonGain = styled.button`
  border: 0;
  background-color: #65e1cb;
  color: white;
  font-size: 1rem;
  margin-right: 50px;
  width: 120px;
  height: 30px;
`;
export const ButtonLose = styled.button`
  border: 0;
  background-color: #f8a388;
  color: white;
  font-size: 1rem;
  margin-right: 0px;
  width: 120px;
  height: 30px;
`;

export const ButtonDone = styled.button`
  border: 0;
  background-color: #5777a8;
  color: white;
  font-size: 1rem;
  margin-right: 0px;
  width: 120px;
  height: 30px;
`;
