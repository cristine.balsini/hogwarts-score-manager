import React from "react";
import "./App.css";
import Houses from "./components/houses";
import StudentTable from "./components/table";

function App() {

  return (
    <div className="App">
      <Houses />
      <StudentTable />
    </div>
  );
}

export default App;
